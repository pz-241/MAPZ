﻿namespace Lab3
{
    public class Farm
    {
        public string Name { get; set; }
        public int Count {  get; set; }
        public Guid FarmID { get; set; }
        public FarmType Type { get; set; }

        public Farm(string name, int count, string type)
        {
            Name = name;
            Count = count;
            FarmID = Guid.NewGuid();
            Type = new FarmType(type);
        }

        public override string ToString()
        {
            return String.Format($"Farm name: {Name}. Type: {Type.TypeFarm}. ID = {FarmID}");
        }
    }



    public class FarmType
    {
        public Guid ID {  get; set; }  // Забрати айді, доступатись по посиланню
        public string TypeFarm {  get; set; }

        public FarmType(string type)
        {
            ID = Guid.NewGuid();
            TypeFarm = type;
        }
    }

#pragma warning disable S1104 // Fields should not have public accessibility

    public class ModelTests 
    {

        public static List<FarmType> types = new List<FarmType>()
        {
                new FarmType("Chickens"),
                new FarmType("Pigs"),
                new FarmType("Cows"),
                new FarmType("Horses"),
                new FarmType("Sheep"),
                new FarmType("Bees")
        };

        public List<Farm> Farms = new List<Farm>() 
        {
            new Farm("Sencha", 1230, types[0].TypeFarm),
            new Farm("Nyva", 28, types[3].TypeFarm),
            new Farm("Pischanska", 262, types[5].TypeFarm),
            new Farm("Lanna-Agro", 36, types[1].TypeFarm),
            new Farm("Ahropraym Kholdynh", 1148, types[4].TypeFarm),
            new Farm("Promin", 8600, types[2].TypeFarm),
            new Farm("Svitanok", 813, types[2].TypeFarm)
        };

        public void ShowFarmsList()
        {
            Farms.ForEach(farm => Console.WriteLine(farm.ToString()));
        }
    }

#pragma warning restore S1104 // Fields should not have public accessibility

    public static class ExtensionMethod
    {
        public static void ChangeType(this Farm farm, string newType)
        {
            farm.Type = new FarmType(newType);
        }

        public static string GetFarmIDAsString(this Farm farm)
        {
            return farm.FarmID.ToString();
        }

        public static void AddToFront<T>(this List<T> list, T item)
        {
            list.Insert(0, item);
        }

    }

    public class FarmComparer : IComparer<Farm>
    {
        public int Compare(Farm firstFarm, Farm secondFarm)
        {
            if (firstFarm == null || secondFarm == null) { return 0; }
            return String.Compare(firstFarm.Name, secondFarm.Name); // return String.Compare(firstFarm?.Name, secondFarm?.Name);   firstFarm?.Name
                                                                    // (firstFarm == null ? null : firstFarm.Name
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            ModelTests model = new ModelTests();

            model.ShowFarmsList();
        }
    }

}
