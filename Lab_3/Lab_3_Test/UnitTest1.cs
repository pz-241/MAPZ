using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Lab3;

namespace Lab3_test
{
    [TestFixture]
    public class NUnitTests
    {
        private ModelTests model;

        [SetUp]
        public void Setup()  // Done
        {
            model = new ModelTests();
        }

        [Test]
        public void TestSelect() // ???????????? ?????? ?????????
        {
            var result = model.Farms.Select(farm => farm.Type).ToArray();

            Assert.That(result[^1], Is.EqualTo(model.Farms[^1].Type));
        }

        [Test]
        public void TestWhere() // ???????????? ?????? ?????????
        {
            var arr = model.Farms.Where(farm => farm.Count < 50).ToArray();
            var lessThan50 = model.Farms.Count(farm => farm.Count < 50);
            
            Assert.That(arr, Has.Length.EqualTo(lessThan50));
        }

        [Test]
        public void TestListOperation()  // ?????? ?? ????? ??????(????????? ? ??????) ????????? ?????? ????? (Add, Insert, Remove, Sort, ?????? ?? ???????)
        {
            var farms = model.Farms.OrderByDescending(f => f.Count).ToList();

            Assert.Multiple(() =>
            {
                Assert.That(farms[0].Count, Is.EqualTo(8600));
                Assert.That(farms[^1].Count, Is.EqualTo(28));
            });
        }

        [Test]
        public void TestDictionaryOperation()  // ?????? ?? ????? ??????(?????????, ?????????, ?????)
        {
            var farmsDict = model.Farms.ToDictionary(
                Farm => Farm.FarmID,
                Farm => Farm
            );

            var existingKey = model.Farms[0].FarmID;
            var nonExistingKey = Guid.NewGuid();

            Assert.That(farmsDict.ContainsKey(existingKey), Is.True);
            Assert.That(farmsDict.ContainsKey(nonExistingKey), Is.False);
        }

        [Test]
        public void TestExtensionMethod()  // ??????????? ?? ????, ????????? ??? ???????? ??'???
        {
            var farm = new Farm(name: "Test Farm", count: 50, type: "Cows");
            farm.ChangeType("Sheep");

            Assert.That(farm.Type.TypeFarm, Is.EqualTo("Sheep"));
        }

        [Test]
        public void TestAnonymousObject()
        {

            var result = model.Farms.Select(farm => new
            {
                farm.Name,
                farm.Type,
            }).ToList();

            Assert.Multiple(() =>
            {
                Assert.That(result[0].Name, Is.EqualTo(model.Farms[0].Name));
                Assert.That(result[0].Type, Is.EqualTo(model.Farms[0].Type));
            });
        }

        [Test]
        public void TestComparer()  // ???? ?????? ??'??? ??? ?? ???????? ??????? -1, ? ???????, ??????? ??? ?????? 
        {
            var comparator = new FarmComparer();

            var sortedFarms = model.Farms.OrderBy(farm => farm, comparator).ToList();

            for (int i = 0; i < sortedFarms.Count - 1; i++)
            {
                Assert.That(comparator.Compare(sortedFarms[i], sortedFarms[i + 1]), Is.LessThanOrEqualTo(0));
            }
        }

        [Test]
        public void TestToArray() 
        {
            Farm[] arr = [.. model.Farms];

            for (int i = 0; i < model.Farms.Count; i++)
            {
                Assert.That(arr[i], Is.EqualTo(model.Farms[i]));
            }

        }

        [Test]
        public void TestSorting()
        {
            var farmsSortedByName = model.Farms
                .OrderBy(farm => farm.Name)
                .Select(farm => farm.Name).ToList();

            Assert.That(farmsSortedByName.Count, Is.EqualTo(model.Farms.Count));
            Assert.That(farmsSortedByName, Is.Ordered);
        }

        [Test]
        public void TestFarmsDictionary()
        {
            var farmsDict = model.Farms.ToDictionary(
                farm => farm.FarmID,
                farm => farm
            );

            Assert.That(farmsDict, Has.Count.EqualTo(model.Farms.Count));
            foreach (var farm in model.Farms)
            {
                Assert.That(farmsDict.ContainsKey(farm.FarmID), Is.True);
            }
        }

        [Test]
        public void TestFarmsQueue()  // ?????? ????? ?????? ?????
        {
            var farmsQueue = new Queue<Farm>(model.Farms);

            Assert.That(farmsQueue, Has.Count.EqualTo(model.Farms.Count));
            Assert.That(farmsQueue.Peek().Name, Is.EqualTo(model.Farms[0].Name));
        }

        [Test]
        public void TestFarmsStack() // ?????? ????? ?????? ?????
        {
            var result = new Stack<Farm>(model.Farms);

            Assert.That(result, Has.Count.EqualTo(model.Farms.Count));
            Assert.That(result.Peek().Name, Is.EqualTo(model.Farms[^1].Name));
        }

        [Test]
        public void TestGroupBy()
        {
            var farmsGroupedByType = model.Farms.GroupBy(farm => farm.Type.TypeFarm);

            foreach (var group in farmsGroupedByType)
            {
                Assert.That(group.All(farm => farm.Type.TypeFarm == group.Key), Is.True);
            }
        }

        [Test]
        public void TestComplicatedOperations()
        {
            var farmsWithHighCounts = model.Farms
                .Where(farm => farm.Count > 1000)
                .OrderByDescending(farm => farm.Count)
                .Select(farm => new { farm.Name, farm.Count });

            Assert.That(farmsWithHighCounts, Is.Not.Null);
            Assert.That(farmsWithHighCounts.Select(farm => farm.Count), Is.Ordered.Descending);
        }

    }
}
