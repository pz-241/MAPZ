﻿using System.Reflection;
using System.Runtime.CompilerServices;
using BenchmarkDotNet.Running;

// Реалізувати ланцюжок наслідування у якому б був звичайний клас, абстрактний
// клас та інтерфейс. 

interface IElecticTransport // Інтерфейс 
{
    void LowerHorns();
}

abstract class ElecticTransport // Абстрактний клас
{
    public int PassengerCapacity;
    public string TransportModel;

    public ElecticTransport(int passengers, string model)
    {
        PassengerCapacity = passengers;
        TransportModel = model;
    }

    public ElecticTransport(int passengers) // Перевантажений конструктор базового класу
    {
        this.PassengerCapacity = passengers;
        TransportModel = "Škoda 14Tr";
    }

/*    public ElecticTransport()
    {
        PassengerCapacity = 100;
        TransportModel = "ElectroLAZ-12";
    }*/

    public abstract void RaiseHorns();
}


internal class Trolleybus : ElecticTransport, IElecticTransport // Звичайний клас. Множинне наслідування
{

    // Внутрішній клас
    public class Engine
    {

    }

    public Engine engine;

    private int NumberOfWheels;
    private int NumberOfHorns;

    public Trolleybus(int passengers, string model, int wheels, int horns) : base(passengers, model)  // Використання base
    {
        NumberOfWheels = wheels;
        NumberOfHorns = horns;
    }

    public Trolleybus(Engine engine, int wheels, int horns) : base(106, "Electron T19102")
    {
        this.engine = engine;
        NumberOfWheels = wheels;
        NumberOfHorns = horns;
    }

    public Trolleybus() : this(100, "ElektroLAZ-12", 6, 2) { }

    public override void RaiseHorns()
    {
        Console.WriteLine("Raising trolleybus horns.");
    }

    void IElecticTransport.LowerHorns()
    {
        Console.WriteLine("Lower trolleybus horns.");
    }

    public void OpenDoor()
    {
        Console.WriteLine("Opening all trolleybus doors.");
    }

    public void OpenDoor(int door)
    {
        Console.WriteLine($"Opening {door} trolleybus door.");
    }

    public static explicit operator string(Trolleybus trolleybus) => trolleybus.TransportModel;

    public static implicit operator int(Trolleybus trolleybus) => trolleybus.PassengerCapacity;

    public override bool Equals(object? obj)
    {
        if (obj is Trolleybus otherTrolleybus)
        {
            return PassengerCapacity == otherTrolleybus.PassengerCapacity && TransportModel == otherTrolleybus.TransportModel &&
                NumberOfWheels == otherTrolleybus.NumberOfWheels && NumberOfHorns == otherTrolleybus.NumberOfHorns;
        }
        return false;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(PassengerCapacity, TransportModel, NumberOfWheels, NumberOfHorns);
    }

    protected object MemberwiseClone()
    {
        // Використовуємо конструктор копіювання для створення копії
        return new Trolleybus(this.PassengerCapacity, this.TransportModel, this.NumberOfWheels, this.NumberOfHorns);
    }

}



// Структури

/*interface IElecticTransport2 
{
    void LowerHorns();
}

abstract struct ElecticTransport2 
{
    protected int PassengerCapacity;
    protected string TransportModel;

    public ElecticTransport2(int passengers, string model)
    {
        PassengerCapacity = passengers;
        TransportModel = model;
    }

    public abstract void RaiseHorns();
}


internal struct Trolleybus2 : ElecticTransport, IElecticTransport 
{
    private int NumberOfWheels;
    private int NumberOfHorns;

    public Trolleybus(int passengers, string model, int wheels, int horns) : base(passengers, model)
    {
    }

    public Trolleybus(Engine engine, int wheels, int horns) : base(106, "Electron T19102")
    {
    }

    void IElecticTransport.LowerHorns()
    {
        Console.WriteLine("Lower trolleybus horns.");
    }
}
*/

class AccessModifiersC
{
    AccessModifiersC() { }
    int field;
    void function() { }

    class NestedClass { }
}

struct AccessModifiersS
{
    int field;
    void function() { }
}

interface IAccessModifiersC
{
    void function() { }
}

enum TrolleybusColor
{
    Yellow = 1,
    Red = 2,
    Blue = 3,
    White = 4,
    ZhKKvitka = 5,
    Green = Blue | Yellow,
    Black = ~White,
    RedAndBlue = Red & Blue,
    BlueAndPink = 6,
    Pink = BlueAndPink ^ Blue,
}

class Initialization
{
    private int dynamicField = 105;
    protected static int staticField = 108;

    public Initialization()
    {
        Console.WriteLine("Dynamic constructor");
    }

    static Initialization()
    {
        ++staticField;
        Console.WriteLine("Static constructor");
    }
}

class InitializationChild : Initialization
{
    public InitializationChild()
    {
        Console.WriteLine("Dynamic child constructor");
    }

    static InitializationChild()
    {
        ++staticField;
        Console.WriteLine("Static child constructor");
    }
}

namespace Lab_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<MethodsBenchmark>();
        }

        static void OverrideObjectMethodsTest()
        {
            Trolleybus trolleybus = new Trolleybus();
            Trolleybus trolleybus1 = new Trolleybus(210, "ElectroLaz-20", 10, 2);
            bool equal = trolleybus.Equals(trolleybus1);
            if (equal)
            {
                Console.WriteLine("Trolleybus equal trolleybus1");
            }
        }

        static void ExplicitAndImplicitTest()
        {
            Trolleybus trolleybus = new Trolleybus();

            string model = (string)trolleybus;
            int passengers = trolleybus;
            Console.WriteLine("Trolleybus model: " + model + "\nPassenger capacity: " + passengers);
        }

        static void BoxAndUnboxTest()
        {
            int lazBox = 110;
            object refLaz = lazBox; // Boxing
            Console.WriteLine("After boxing: " + refLaz);

            int lazUnbox = (int)refLaz; // Unboxing
            Console.WriteLine("After unboxing: " +  lazUnbox);
        }

        static void RefAndOutTest()
        {
            int name = 113;
            Change(name);
            Console.WriteLine("Default change = " +  name);

            int refName = 135;
            RefChange(ref refName);
            Console.WriteLine("Ref change = " + refName);

            int outName = 168;
            OutChange (out outName);
            Console.WriteLine("Out change = " + outName);
        }

        static void Change(int value)
        {
            value = 105;
        }

        static void RefChange(ref int value)
        {
            value = 104;
        }

        static void OutChange(out int value)
        {
            value = 108;
        }

        static void InitializationTest()
        {
            InitializationChild child = new InitializationChild();
        }

        static void EnumTest()
        {
            TrolleybusColor trolleybusColor = TrolleybusColor.Red & TrolleybusColor.Blue;
            Console.WriteLine(trolleybusColor);
            trolleybusColor = TrolleybusColor.Blue | TrolleybusColor.Yellow;
            Console.WriteLine(trolleybusColor);
            trolleybusColor = TrolleybusColor.White;
            Console.WriteLine(trolleybusColor);
            trolleybusColor = TrolleybusColor.BlueAndPink ^ TrolleybusColor.Blue;
            Console.WriteLine(trolleybusColor);
        }

        static void TrolleybusTest()
        {
            Trolleybus trolleybus = new Trolleybus();
            
            trolleybus.TransportModel = "ElectroLAZ-20";
        }
    }
}