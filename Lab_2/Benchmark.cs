﻿using BenchmarkDotNet.Attributes;
using Microsoft.Diagnostics.Tracing.Parsers.MicrosoftWindowsWPF;
using System.Runtime.CompilerServices;

[MemoryDiagnoser]
public class MethodsBenchmark
{

    [Benchmark]
    public void TestPrivateMethod()
    {
        MethodsTest.BenchAccessors.BenchPrivateMethod();
    }

    [Benchmark]
    public void TestProtectedMethod()
    {
        MethodsTest.BenchAccessors.BenchProtectedMethod();
    }

    [Benchmark]
    public void TestPublicMethod()
    {
        MethodsTest.BenchAccessors.BenchPublicMethod();
    }


    [Benchmark]
    public void TestNestedPrivateMethod()
    {
        MethodsTest.NestedClass.BenchAccessors.BenchPrivateMethod();
    }

    [Benchmark]
    public void TestNestedProtectedMethod()
    {
        MethodsTest.NestedClass.BenchAccessors.BenchProtectedMethod();
    }

    [Benchmark]
    public void TestNestedPublicMethod()
    {
        MethodsTest.NestedClass.BenchAccessors.BenchPublicMethod();
    }
}


public class MethodsTest
{
    int privateCounter = 0;
    int protectedCounter = 0;
    int publicCounter = 0;

    [MethodImplAttribute(MethodImplOptions.NoInlining)]
    private void PrivateMethod()
    {
        privateCounter++;
    }
    [MethodImplAttribute(MethodImplOptions.NoInlining)]
    protected void ProtectedMethod()
    {
        protectedCounter++;
    }
    [MethodImplAttribute(MethodImplOptions.NoInlining)]
    public void PublicMethod()
    {
        publicCounter++;
    }

    public class BenchAccessors
    {
        private const int iterations = 10000000;

        static public void BenchPrivateMethod()
        {
            MethodsTest obj = new MethodsTest();
            for (int i = 0; i < iterations; i++)
            {
                obj.PrivateMethod();
            }
        }
        static public void BenchProtectedMethod()
        {
            MethodsTest obj = new MethodsTest();
            for (int i = 0; i < iterations; i++)
            {
                obj.ProtectedMethod();
            }
        }
        static public void BenchPublicMethod()
        {
            MethodsTest obj = new MethodsTest();
            for (int i = 0; i < iterations; i++)
            {
                obj.PublicMethod();
            }
        }
    }

    public class NestedClass
    {
        int privateCounter = 0;
        int protectedCounter = 0;
        int publicCounter = 0;

        [MethodImplAttribute(MethodImplOptions.NoInlining)]
        private void NestedPrivateMethod()
        {
            privateCounter++;
        }
        [MethodImplAttribute(MethodImplOptions.NoInlining)]
        protected void NestedProtectedMethod()
        {
            protectedCounter++;
        }
        [MethodImplAttribute(MethodImplOptions.NoInlining)]
        public void NestedPublicMethod()
        {
            publicCounter++;
        }

        public class BenchAccessors
        {
            private const int iterations = 10000000;

            static public void BenchPrivateMethod()
            {
                NestedClass obj = new NestedClass();
                for (int i = 0; i < iterations; i++)
                {
                    obj.NestedPrivateMethod();
                }
            }
            static public void BenchProtectedMethod()
            {
                NestedClass obj = new NestedClass();
                for (int i = 0; i < iterations; i++)
                {
                    obj.NestedProtectedMethod();
                }
            }
            static public void BenchPublicMethod()
            {
                NestedClass obj = new NestedClass();
                for (int i = 0; i < iterations; i++)
                {
                    obj.NestedPublicMethod();
                }
            }
        }
    }
}