﻿using Lab_6;

namespace Lab6
{
    public interface ICommand
    {
        void Execute();
    }

    public class SaveFarmCommand : ICommand
    {
        Farm _farm = Farm.Instance;

        public SaveFarmCommand(Farm farm)
        {
            _farm = farm;
        }

        public void Execute()
        {
            _farm.Save();
        }
    }

    public class ClearFarmCommand : ICommand
    {
        Farm _farm = Farm.Instance;

        public ClearFarmCommand(Farm farm)
        {
            _farm = farm;
        }

        public void Execute()
        {
            _farm.Clear();
        }
    }
}
