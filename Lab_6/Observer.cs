﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    public interface IListener
    {
        void Update();
    }

    public class Observer
    {
        private List<IListener> listeners = [];

        public void Subscribe(IListener listener)
        {
            listeners.Add(listener);
        }
        public void Unsubscribe(IListener listener)
        {
            listeners.Remove(listener);
        }

        public void Notify()
        {
            foreach (var item in listeners)
            {
                item.Update();
            }
        }
    }
}
