﻿using Lab_6;

namespace Lab6
{
    public interface IProfileState
    {
        void SetFarm(Profile farm);
        void Bought();
        void Sold();
        string GetState();
    }

    public class EmptyState : IProfileState
    {
        private Profile _profile;

        public void SetFarm(Profile profile)
        {
            _profile = profile;
        }

        public void Bought()
        {
            _profile.TransitTo(new BoughtState());
        }

        public void Sold() { }

        public string GetState()
        {
            return "Empty";
        }
    }

    public class BoughtState : IProfileState
    {
        private Profile _profile;

        public void SetFarm(Profile profile)
        {
            _profile = profile;
        }

        public void Bought() { }

        public void Sold()
        {
            _profile.TransitTo(new SoldState());
        }

        public string GetState()
        {
            return "Bought";
        }
    }

    public class SoldState : IProfileState
    {
        private Profile _profile;

        public void SetFarm(Profile profile)
        {
            _profile = profile;
        }

        public void Bought()
        {
            _profile.TransitTo(new BoughtState());
        }

        public void Sold() { }

        public string GetState()
        {
            return "Sold";
        }
    }
}