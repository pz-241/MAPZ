﻿using System.IO.Packaging;
using System.Text;

namespace Lab_6
{

    public interface IComponent
    {
        string Collect(int d);  
        string Print(int d);
    }

    public class Potato : IComponent
    {
        public String Name { get; } = "potato";

        public string Collect(int d)    
        {
            return new string('*', d) + $"Collecting {Name}";
        }

        public string Print(int d)
        {
            return new string('*', d) + Name;
        }

    }

    public class Carrot : IComponent
    {
        public string Name { get; } = "carrot";

        public string Collect(int d)
        {
            return new string('*', d) + $"Collecting {Name}";
        }

        public string Print(int d)
        {
            return new string('*', d) + Name;
        }

    }

    public class Tomato : IComponent
    {
        public string Name { get; } = "tomato";

        public string Collect(int d)
        {
            return new string('*', d) + $"Collecting {Name}";
        }

        public string Print(int d)
        {
            return new string('*', d) + Name;
        }

    }

    public class Cucumber : IComponent
    {
        public string Name { get; } = "cucumber";

        public string Collect(int d)
        {
            return new string('*', d) + $"Collecting {Name}";
        }
        public string Print(int d)
        {
            return new string('*', d) + Name;
        }

    }

    public class Composite : IComponent
    {
        public string Name { get { return _name; } }
        string _name;
        private List<IComponent> components = new();

        public Composite(string name)
        {
            _name = name;
        }

        public Composite()
        {
            _name = String.Empty;
        }

        public void AddComponent(IComponent component)
        {
            components.Add(component);
        }

        public string Collect(int d)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(new string('*', d) + $"Collecting {Name}...");
            foreach (IComponent component in components)
            {
                sb.AppendLine(component.Collect(d + 1));
            }
            return sb.ToString();

        }

        public string Print(int d = 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(new string('*', d) + Name);
            foreach (IComponent component in components)
            {
                sb.AppendLine(component.Print(d + 1));
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            return Print(0);
        }

    }
}