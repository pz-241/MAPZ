﻿using Lab_6;

namespace Lab6
{
    public class Profile
    {
        private IProfileState _state = null;
        Guid guid = new();

        public string GetState => _state.GetState();

        public Profile(IProfileState state)
        {
            this.TransitTo(state);
        }

        public void TransitTo(IProfileState state)
        {
            _state = state;
            _state.SetFarm(this);
        }

        public void Bought()
        {
            _state.Bought();
        }

        public void Sold()
        {
            _state.Sold();
        }



    }
}