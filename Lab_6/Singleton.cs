﻿using Lab6;
using System.Windows;

namespace Lab_6
{
    public class Farm : IListener
    {
        private static Farm? instance;
        public string Name { get; set; } = string.Empty;
        public bool CowBarn { get; set; } = false;
        public bool PigBarn { get; set; } = false;
        public bool ChickenBarn { get; set; } = false;
        public List<IAnimal> Cows { get; set; } = [];
        public List<IAnimal> Pigs { get; set; } = [];
        public List<IAnimal> Chickens { get; set; } = [];
        public uint Level { get; set; } = 0;
        public IComponent Composite { get; set; }

        private Farm() { }

        public static Farm Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Farm();
                }
                return instance;
            }
        }

        public void addAnimal(IAnimal animal)
        {
            if (animal is Cow)
            {
                this.Cows.Add(animal);
            }
            if (animal is Pig)
            {
                this.Pigs.Add(animal);
            }
            if (animal is Chicken)
            {
                this.Chickens.Add(animal);
            }
        }

        public void ClearData()
        {
            Name = string.Empty;
            CowBarn = false;
            PigBarn = false;
            ChickenBarn = false;
            Cows.Clear();
            Pigs.Clear();
            Chickens.Clear();
            Level = 0;
            Composite = null;
        }

        public void Clear()
        {
            Name = string.Empty;
            CowBarn = false;
            PigBarn = false;
            ChickenBarn = false;
            Cows.Clear();
            Pigs.Clear();
            Chickens.Clear();
            Level = 0;
            Composite = null;

            MessageBox.Show("Farm was cleared", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
        }


        public void Save()
        {
            MessageBox.Show("Farm was saved", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void Update()
        {
            Level++;
        }
    }

}