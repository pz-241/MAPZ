﻿using Lab6;
using System.Collections.Specialized;
using System.Windows;

namespace Lab_6
{
    public interface IAnimal
    {
        void Buy();
    }

    abstract class Animal
    {
        public Observer _eventManager;
        Farm _farm;
        public string Species { get; set; }
        public int Legs { get; set; }
        public string Sound { get; set; }


        public Animal(string species, int legs, string sound)
        {
            Species = species;
            Legs = legs;
            Sound = sound;
            _eventManager = new Observer();
            _farm = Farm.Instance;
            _eventManager.Subscribe(_farm);
        }

        public Animal(Animal other)
        {
            this.Species = other.Species;
            this.Legs = other.Legs;
            this.Sound = other.Sound;
        }

        public abstract IAnimal Clone(); // Клонування вертає лівий об'єкт, перенести в IAnimal
    }

    // Конкретні класи для різних типів тварин
    class Cow : Animal, IAnimal
    {
        public int horns;
        public void Buy()
        {
            Console.WriteLine("The cow was bought");
        }
        public Cow() : base("Mammal", 4, "Mu")
        {
            this.horns = 2;
        }

        // Prototype
        public Cow(Cow other) : base(other)
        {
            this.horns = other.horns;
        }
        public override IAnimal Clone()
        {
            _eventManager.Notify();
            return new Cow (this);
        }
    }

    class Chicken : Animal, IAnimal
    {
        public int wings;
        public void Buy()
        {
            Console.WriteLine("The chicken was bought");
        }
        public Chicken() : base("Bird", 2, "Kukariku")
        {
            this.wings = 2;
        }

        // Prototype
        public Chicken(Chicken other) : base(other)
        {
            this.wings = other.wings;
        }
        public override IAnimal Clone()
        {
            _eventManager.Notify();
            return new Chicken (this);
        }
    }

    class Pig : Animal, IAnimal { 
        public void Buy()
        {
            Console.WriteLine("The pig was bought");
        }
        public Pig() : base("Mammal", 4, "Hru") { }

        // Prototype
        public Pig(Pig other) : base(other) { }

        public override IAnimal Clone()
        {
            _eventManager.Notify();
            return new Pig(this);
        }
    }

    // Інтерфейс для фабрик тварин
    public abstract class AnimalFactory
    {
        public Observer _eventManager;
        Farm _farm;
        public abstract IAnimal CreateAnimal();

        public AnimalFactory()
        {
            _eventManager = new Observer();
            _farm = Farm.Instance;
            _eventManager.Subscribe(_farm);
        }
    }

    // Конкретні фабрики для різних типів тварин
    class CowFactory : AnimalFactory
    {
        public override IAnimal CreateAnimal()
        {
            Console.WriteLine("Cow was created");
            _eventManager.Notify();
            return new Cow();
        }
    }

    class ChickenFactory : AnimalFactory
    {
        public override IAnimal CreateAnimal()
        {
            Console.WriteLine("Chicken was created");
            _eventManager.Notify();
            return new Chicken();
        }
    }

    class PigFactory : AnimalFactory
    {
        public override IAnimal CreateAnimal()
        {
            Console.WriteLine(" Pig was created");
            _eventManager.Notify();
            return new Pig();
        }
    }

    // Клас Ферми
    public class AnimalsFactory
    {
        private Dictionary<string, AnimalFactory> factoriesDictionary = [];

        public AnimalsFactory()
        {
            foreach (var type in typeof(AnimalsFactory).Assembly.GetTypes())
            {
                if (typeof(AnimalFactory).IsAssignableFrom(type) && !type.IsInterface)
                {
                    string factoryName = type.Name.Replace("Factory", string.Empty);
                    AnimalFactory factoryInstance = (AnimalFactory)Activator.CreateInstance(type);
                    factoriesDictionary.Add(factoryName, factoryInstance);
                }
            }
        }

        public IAnimal BuyAnimal(string key)
        {
            return factoriesDictionary[key].CreateAnimal();
        }

    }
}