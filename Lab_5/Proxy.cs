﻿using System.Runtime;
using System.Windows;
using System.Windows.Controls;

namespace Lab_5
{
    public class Proxy : IFarmFacade
    {
        private FarmFacade _farmFacade;

        public Proxy()
        {
            _farmFacade = new FarmFacade();
        }   
    
        public void AddAnimalToAllBarns()
        {
            var farm = Farm.Instance;
            if (farm.Level > 10
                && _farmFacade != null)
            {
                _farmFacade.AddAnimalToAllBarns();
            }   
            else
            {
                MessageBox.Show("Low level!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        public void CreateAllBarns()
        {
            var farm = Farm.Instance;
            if (farm.CowBarn && farm.PigBarn && farm.ChickenBarn)
            {
                MessageBox.Show("All barn exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (_farmFacade != null)
            {
                _farmFacade.CreateAllBarns();
            }
            else
            {
                MessageBox.Show("Unknown error!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        public void CreateFarmWithAllBarns(string name)
        {
            var farm = Farm.Instance;
            if (farm.Name != null)
            {
                MessageBox.Show("Farm exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (name.Length < 5)
            {
                MessageBox.Show("Farm name should be at least 5 characters long!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (_farmFacade != null)
            {
                _farmFacade.CreateFarmWithAllBarns(name);
            }
            else
            {
                MessageBox.Show("Unknown error!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }
    }
}