﻿using System;
using System.Security.AccessControl;

namespace Lab_5
{
    public interface IFarmFacade
    {
        void AddAnimalToAllBarns();
        void CreateFarmWithAllBarns(string name);
        void CreateAllBarns();
    }
        
    public class FarmFacade : IFarmFacade
    {
        public void AddAnimalToAllBarns()   
        {
            var farm = Farm.Instance;
            IAnimalFactory factory;
            IAnimal item;

            factory = new ChickenFactory();
            item = factory.CreateAnimal();
            farm.addAnimal(item);

            factory = new PigFactory();
            item = factory.CreateAnimal();
            farm.addAnimal(item);

            factory = new CowFactory();
            item = factory.CreateAnimal();
            farm.addAnimal(item);
        }

        public void CreateAllBarns()
        {
            var farm = Farm.Instance;
            
            farm.ChickenBarn = true;
            farm.PigBarn = true;
            farm.CowBarn = true;
        }

        public void CreateFarmWithAllBarns(string name)
        {
            var farm = Farm.Instance;
            farm.Name = name;

            farm.ChickenBarn = true;
            farm.PigBarn = true;
            farm.CowBarn = true;
        }

        public void SetSector(string sector)
        {
            var farm = Farm.Instance;

            switch (sector)
            {
                case "Sector 1":
                    farm.Composite = CreateSec1();
                    break;
                case "Sector 2":
                    farm.Composite = CreateSec2();
                    break;
                case "All":
                    farm.Composite = CreateAllSec();
                    break;
            }
        }

        public IComponent CreateSec1()
        {
            var sector = new Composite("Sector 1");

            var ps = new Composite("PotatoSector");
            ps.AddComponent(new Potato());
            sector.AddComponent(ps);

            var cs = new Composite("CarrotSector");
            cs.AddComponent(new Carrot());
            sector.AddComponent(cs);

            return sector;
        }

        public IComponent CreateSec2()
        {
            var sector = new Composite("Sector 2");

            var ts = new Composite("TomatoSector");
            ts.AddComponent(new Tomato());
            sector.AddComponent(ts);

            var cs = new Composite("CucumberSector");
            cs.AddComponent(new Cucumber());
            sector.AddComponent(cs);

            return sector;
        }

        public IComponent CreateAllSec()
        {
            var sector = new Composite("All sector");

            var ts = new Composite("TomatoSector");
            ts.AddComponent(new Tomato());
            sector.AddComponent(ts);

            var cs = new Composite("CucumberSector");
            cs.AddComponent(new Cucumber());
            sector.AddComponent(cs);

            var ps = new Composite("PotatoSector");
            ps.AddComponent(new Potato());
            sector.AddComponent(ps);

            var carrots = new Composite("CarrotSector");
            carrots.AddComponent(new Carrot());
            sector.AddComponent(carrots);

            return sector;
        }
    }
}

