﻿using System.Data.Common;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Lab_5.MainWindow;

namespace Lab_5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    { 

        public IAnimalFactory factory;
        private Proxy farmProxy = new Proxy();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void CreateFarm_btn_Click(object sender, RoutedEventArgs e)
        {
            if (name_TextBox.Text == string.Empty)
            {
                MessageBox.Show("Enter farm name!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var name = name_TextBox.Text;
            var farm = Farm.Instance;
            farm.Name = name;
            ListBoxTextUpdate();
        }

        private void CreateBarn_btn_click(object sender, RoutedEventArgs e)
        {
            var farm = Farm.Instance;
            if (farm.Name == string.Empty)
            {
                MessageBox.Show("There is no farm!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (type_ComboBox.Text == string.Empty)
            {
                MessageBox.Show("Enter animal type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var barnType = type_ComboBox.Text;

            bool barnExists = false;
            switch (barnType)
            {
                case "Cows":
                    barnExists = farm.CowBarn;
                    break;

                case "Chickens":
                    barnExists = farm.ChickenBarn;
                    break;

                case "Pigs":
                    barnExists = farm.PigBarn;
                    break;

                default:
                    // Ви можете виконати певні дії, якщо відсутній відповідний тип тварини
                    MessageBox.Show("Invalid barn type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
            }

            if (barnExists)
            {
                MessageBox.Show("Barn already exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            AddBarn(barnType);

            switch (barnType)
            {
                case "Cows":
                    farm.CowBarn = true;
                    break;

                case "Chickens":
                    farm.ChickenBarn = true;
                    break;

                case "Pigs":
                    farm.PigBarn = true;
                    break;

                default:
                    // Ви можете виконати певні дії, якщо відсутній відповідний тип тварини
                    MessageBox.Show("Invalid barn type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
            }
            ListBoxTextUpdate();

        }

        private void Buy_btn_Click(object sender, RoutedEventArgs e)
        {
            BuyAnimal();
            ListBoxTextUpdate();
        }

        public void BuyAnimal()
        {
            var farm = Farm.Instance;
            if (farm.Name == string.Empty)
            {
                MessageBox.Show("There is no farm!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (animal_ComboBox.Text == string.Empty)
            {
                MessageBox.Show("Enter animal type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            IAnimal item;

            switch (animal_ComboBox.Text)
            {
                case "Cow":

                    if (farm.CowBarn == false)
                    {
                        MessageBox.Show("Barn does not exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (farm.Cows.Count == 0)
                    {
                        // Якщо список корів порожній, використовуємо фабрику для створення
                        factory = new CowFactory();
                        item = factory.CreateAnimal();
                    }
                    else
                    {
                        // Якщо список не порожній, клонуємо перший елемент
                        item = ((Animal)farm.Cows[0]).Clone();
                    }
                    break;

                case "Chicken":

                    if (farm.ChickenBarn == false)
                    {
                        MessageBox.Show("Barn does not exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (farm.Chickens.Count == 0)
                    {
                        // Якщо список курей порожній, використовуємо фабрику для створення
                        factory = new ChickenFactory();
                        item = factory.CreateAnimal();
                    }
                    else
                    {
                        // Якщо список не порожній, клонуємо перший елемент
                        item = ((Animal)farm.Chickens[0]).Clone();
                    }
                    break;

                case "Pig":

                    if (farm.PigBarn == false)
                    {
                        MessageBox.Show("Barn does not exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (farm.Pigs.Count == 0)
                    {
                        // Якщо список свиней порожній, використовуємо фабрику для створення
                        factory = new PigFactory();
                        item = factory.CreateAnimal();
                    }
                    else
                    {
                        // Якщо список не порожній, клонуємо перший елемент
                        item = ((Animal)farm.Pigs[0]).Clone();
                    }
                    break;

                default:

                    // Ви можете виконати певні дії, якщо відсутній відповідний тип тварини
                    item = null;
                    break;
            }

            farm.addAnimal(item);
        }

        public void AddBarn(string type)
        {
            Farm farm = Farm.Instance;

            switch (type)
            {
                case "Cows":
                    farm.CowBarn = true;
                    break;
                case "Pigs":
                    farm.PigBarn = true;
                    break;
                case "Chickens":
                    farm.ChickenBarn = true;
                    break;
                default:
                    throw new ArgumentException("Invalid barn type!", nameof(type));
            }
        }

        public void ListBoxTextUpdate()
        {
            var farm = Farm.Instance;
            GeneralListBox.Items.Clear();
            GeneralListBox.Items.Add("Farm name: " + farm.Name);
            GeneralListBox.Items.Add("Farm level: " + farm.Level);
            if (farm.CowBarn == true)
            {
                GeneralListBox.Items.Add("Cow barn: " + farm.Cows.Count + " cows.");
            }
            if (farm.PigBarn == true)
            {
                GeneralListBox.Items.Add("Pig barn: " + farm.Pigs.Count + " pigs.");
            }
            if (farm.ChickenBarn == true)
            {
                GeneralListBox.Items.Add("Chicken barn: " + farm.Chickens.Count + " chickens.");
            }
            if (farm.Composite != null)
            {
                GeneralListBox.Items.Add(farm.Composite.ToString());
            }
        }

        private void BuyToAllBarns_btn_Click(object sender, RoutedEventArgs e)
        {
            var farm = Farm.Instance;
            if (!farm.ChickenBarn || !farm.CowBarn || !farm.PigBarn)
            {
                MessageBox.Show("Some barn does not exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            farmProxy.AddAnimalToAllBarns();
            ListBoxTextUpdate();
        }

        private void CreateAllBarns_btn_Click(object sender, RoutedEventArgs e)
        {
            var farm = Farm.Instance;
            if (farm.Name == string.Empty)
            {
                MessageBox.Show("There is no farm!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            farmProxy.CreateAllBarns();
            ListBoxTextUpdate();
        }

        private void CreateFarmAndBarns_btn_Click(object sender, RoutedEventArgs e)
        {
            if (name_TextBox.Text == string.Empty)
            {
                MessageBox.Show("Enter farm name!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            farmProxy.CreateFarmWithAllBarns(name_TextBox.Text);

            ListBoxTextUpdate();
        }

        private void Composite_btn_Click(object sender, RoutedEventArgs e)
        {
            var farm = Farm.Instance;
            FarmFacade facade = new FarmFacade();

            facade.SetSector(composite_ComboBox.Text);
            ListBoxTextUpdate();
        }
    }
}