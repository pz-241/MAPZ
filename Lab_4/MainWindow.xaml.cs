﻿using System.Data.Common;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Lab_4.MainWindow;

namespace Lab_4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // Юзання фабрики переробити, наслідування тварини від IAnimal або Animal 

        public const string PIGPATH = "D:\\Study\\Terms\\4 term\\MAPZ\\Lab_4\\Images\\pig_icon.png";
        public const string COWPATH = "D:\\Study\\Terms\\4 term\\MAPZ\\Lab_4\\Images\\cow_icon.png";
        public const string CHICKENPATH = "D:\\Study\\Terms\\4 term\\MAPZ\\Lab_4\\Images\\chicken_icon.png";
        public const string SOILPATH = "D:\\Study\\Terms\\4 term\\MAPZ\\Lab_4\\Images\\soil_icon.png";
        public const string SOILSEEDPATH = "D:\\Study\\Terms\\4 term\\MAPZ\\Lab_4\\Images\\soilSeed_icon.png";
        public const string HAYPATH = "D:\\Study\\Terms\\4 term\\MAPZ\\Lab_4\\Images\\hay_icon.png";

        public IAnimalFactory factory;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void CreateFarm_btn_Click(object sender, RoutedEventArgs e)
        {
            if (name_TextBox.Text == string.Empty)
            {
                MessageBox.Show("Enter farm name!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var name = name_TextBox.Text;
            var farm = Farm.Instance;
            farm.Name = name;
            DrawLabel(farm.Name, new Point(30, 10), 30);
        }

        private void CreateBarn_btn_click(object sender, RoutedEventArgs e)
        {
            var farm = Farm.Instance;
            if (farm.Name == string.Empty)
            {
                MessageBox.Show("There is no farm!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (type_ComboBox.Text == string.Empty)
            {
                MessageBox.Show("Enter animal type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var barnType = type_ComboBox.Text;

            bool barnExists = false;
            switch (barnType)
            {
                case "Cows":
                    barnExists = farm.CowBarn;
                    break;

                case "Chickens":
                    barnExists = farm.ChickenBarn;
                    break;

                case "Pigs":
                    barnExists = farm.PigBarn;
                    break;

                default:
                    // Ви можете виконати певні дії, якщо відсутній відповідний тип тварини
                    MessageBox.Show("Invalid barn type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
            }

            if (barnExists)
            {
                MessageBox.Show("Barn already exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var path = ParseType(barnType);
            AddBarn(barnType);
            Point point = SelectPoint(barnType);
            DrawPhoto(path, point);

            switch (barnType)
            {
                case "Cows":
                    farm.CowBarn = true;
                    break;

                case "Chickens":
                    farm.ChickenBarn = true;
                    break;

                case "Pigs":
                    farm.PigBarn = true;
                    break;

                default:
                    // Ви можете виконати певні дії, якщо відсутній відповідний тип тварини
                    MessageBox.Show("Invalid barn type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
            }

        }

        private void Buy_btn_Click(object sender, RoutedEventArgs e)
        {
            var farm = Farm.Instance;
            if (farm.Name == string.Empty)
            {
                MessageBox.Show("There is no farm!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (animal_ComboBox.Text == string.Empty)
            {
                MessageBox.Show("Enter animal type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string path;
            IAnimal item;

            switch (animal_ComboBox.Text)
            {
                case "Cow":

                    if (farm.CowBarn == false)
                    {
                        MessageBox.Show("Barn does not exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (farm.Cows.Count == 0)
                    {
                        // Якщо список корів порожній, використовуємо фабрику для створення
                        factory = new CowFactory();
                        item = factory.CreateAnimal();
                    }
                    else
                    {
                        // Якщо список не порожній, клонуємо перший елемент
                        item = ((Animal)farm.Cows[0]).Clone();
                    }
                    path = COWPATH;
                    break;

                case "Chicken":

                    if (farm.ChickenBarn == false)
                    {
                        MessageBox.Show("Barn does not exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (farm.Chickens.Count == 0)
                    {
                        // Якщо список курей порожній, використовуємо фабрику для створення
                        factory = new ChickenFactory();
                        item = factory.CreateAnimal();
                    }
                    else
                    {
                        // Якщо список не порожній, клонуємо перший елемент
                        item = ((Animal)farm.Chickens[0]).Clone();
                    }
                    path = CHICKENPATH;
                    break;

                case "Pig":

                    if (farm.PigBarn == false)
                    {
                        MessageBox.Show("Barn does not exist!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (farm.Pigs.Count == 0)
                    {
                        // Якщо список свиней порожній, використовуємо фабрику для створення
                        factory = new PigFactory();
                        item = factory.CreateAnimal();
                    }
                    else
                    {
                        // Якщо список не порожній, клонуємо перший елемент
                        item = ((Animal)farm.Pigs[0]).Clone();
                    }
                    path = PIGPATH;
                    break;

                default:

                    // Ви можете виконати певні дії, якщо відсутній відповідний тип тварини
                    item = null;
                    path = null;
                    break;
            }

            farm.addAnimal(item);

            Point point = RandomPoint(animal_ComboBox.Text);

            DrawAnimal(path, point);
        }

        public void AddBarn(string type)
        {
            Farm farm = Farm.Instance;

            switch (type)
            {
                case "Cows":
                    farm.CowBarn = true;
                    break;
                case "Pigs":
                    farm.PigBarn = true;
                    break;
                case "Chickens":
                    farm.ChickenBarn = true;
                    break;
                default:
                    throw new ArgumentException("Invalid barn type!", nameof(type));
            }
        }

        public Point RandomPoint(string type)
        {
            Random random = new Random();
            double randomX = random.Next(151);
            double randomY = random.Next(151);
            switch (type)
            {
                case "Cow":
                    return new Point(40 + randomX, 90 + randomY);
                case "Pig":
                    return new Point(240 + randomX, 90 + randomY);
                case "Chicken":
                    return new Point(440 + randomX, 90 + randomY);
                default:
                    throw new ArgumentException("Invalid barn type!", nameof(type));
            }
        }

        public Point SelectPoint(string type)
        {
            switch (type)
            {
                case "Cows":
                    return new Point(40, 90);
                case "Pigs":
                    return new Point(240, 90);
                case "Chickens":
                    return new Point(440, 90);
                default:
                    throw new ArgumentException("Invalid barn type!", nameof(type));
            }
        }

        public string ParseType(string type)
        {
            switch (type)
            {
                case "Cows":
                    return HAYPATH;
                case "Pigs":
                    return SOILPATH;
                case "Chickens":
                    return SOILSEEDPATH;
                default:
                    throw new ArgumentException("Invalid barn type!", nameof(type));
            }
        }

        public void DrawAnimal(string path, Point point)
        {
            BitmapImage bitmap = new BitmapImage(new Uri(path));
            Image image = new Image();
            image.Source = bitmap;
            image.Width = 50;
            image.Height = 50;
            Canvas.SetLeft(image, point.X);
            Canvas.SetTop(image, point.Y);
            canvas.Children.Add(image);
        }

        public void DrawPhoto(string path, Point point)
        {
            BitmapImage bitmap = new BitmapImage(new Uri(path));
            Image image = new Image();
            image.Source = bitmap;
            image.Width = 200;
            image.Height = 200;
            Canvas.SetLeft(image, point.X);
            Canvas.SetTop(image, point.Y);
            canvas.Children.Add(image);
        }


        public void DrawLabel(string text, Point point, int font)
        {
            TextBlock textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.FontSize = font;

            Canvas.SetLeft(textBlock, point.X);
            Canvas.SetTop(textBlock, point.Y);

            canvas.Children.Add(textBlock);
        }

    }
}