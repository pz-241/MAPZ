﻿namespace Lab_4
{
    public class Farm
    {
        private static Farm? instance;
        public string Name { get; set; } = string.Empty;
        public bool CowBarn { get; set; } = false;
        public bool PigBarn { get; set; } = false;
        public bool ChickenBarn { get; set; } = false;
        public List<IAnimal> Cows { get; set; } = [];
        public List<IAnimal> Pigs { get; set; } = [];
        public List<IAnimal> Chickens { get; set; } = [];
                
        private Farm() { }

        public static Farm Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Farm();
                }
                return instance;
            }
        }

        public void addAnimal(IAnimal animal)
        {
            if (animal is Cow)
            {
                this.Cows.Add(animal);
            }
            if (animal is Pig)
            {
                this.Pigs.Add(animal);
            }
            if (animal is Chicken)
            {
                this.Chickens.Add(animal);
            }
        }
    }

}