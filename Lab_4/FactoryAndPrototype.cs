﻿using System.Collections.Specialized;
using System.Windows;

namespace Lab_4
{
    public interface IAnimal
    {
        void Buy();
    }

    abstract class Animal
    {
        public string Species { get; set; }
        public int Legs { get; set; }
        public string Sound { get; set; }


        public Animal(string species, int legs, string sound)
        {
            Species = species;
            Legs = legs;
            Sound = sound;
        }

        public Animal(Animal other)
        {
            this.Species = other.Species;
            this.Legs = other.Legs;
            this.Sound = other.Sound;
        }

        public abstract IAnimal Clone(); // Клонування вертає лівий об'єкт, перенести в IAnimal
    }

    // Конкретні класи для різних типів тварин
    class Cow : Animal, IAnimal
    {
        public int horns;
        public void Buy()
        {
            Console.WriteLine("The cow was bought");
        }
        public Cow() : base("Mammal", 4, "Mu")
        {
            this.horns = 2;
        }

        // Prototype
        public Cow(Cow other) : base(other)
        {
            this.horns = other.horns;
        }
        public override IAnimal Clone()
        {
            return new Cow (this);
        }
    }

    class Chicken : Animal, IAnimal
    {
        public int wings;
        public void Buy()
        {
            Console.WriteLine("The chicken was bought");
        }
        public Chicken() : base("Bird", 2, "Kukariku")
        {
            this.wings = 2;
        }

        // Prototype
        public Chicken(Chicken other) : base(other)
        {
            this.wings = other.wings;
        }
        public override IAnimal Clone()
        {
            return new Chicken (this);
        }
    }

    class Pig : Animal, IAnimal { 
        public void Buy()
        {
            Console.WriteLine("The pig was bought");
        }
        public Pig() : base("Mammal", 4, "Hru") { }

        // Prototype
        public Pig(Pig other) : base(other) { }

        public override IAnimal Clone()
        {
            return new Pig(this);
        }
    }

    // Інтерфейс для фабрик тварин
    public interface IAnimalFactory
    {
        IAnimal CreateAnimal();
    }

    // Конкретні фабрики для різних типів тварин
    class CowFactory : IAnimalFactory
    {
        public IAnimal CreateAnimal()
        {
            Console.WriteLine("Cow was created");
            return new Cow();
        }
    }

    class ChickenFactory : IAnimalFactory
    {
        public IAnimal CreateAnimal()
        {
            Console.WriteLine("Chicken was created");
            return new Chicken();
        }
    }

    class PigFactory : IAnimalFactory
    {
        public IAnimal CreateAnimal()
        {
            Console.WriteLine(" Pigwas created");
            return new Pig();
        }
    }

    // Клас Ферми
    public class AnimalFactory
    {
        private Dictionary<string, IAnimalFactory> factoriesDictionary = [];

        public AnimalFactory()
        {
            foreach (var type in typeof(AnimalFactory).Assembly.GetTypes())
            {
                if (typeof(IAnimalFactory).IsAssignableFrom(type) && !type.IsInterface)
                {
                    string factoryName = type.Name.Replace("Factory", string.Empty);
                    IAnimalFactory factoryInstance = (IAnimalFactory)Activator.CreateInstance(type);
                    factoriesDictionary.Add(factoryName, factoryInstance);
                }
            }
        }

        public IAnimal BuyAnimal(string key)
        {
            return factoriesDictionary[key].CreateAnimal();
        }

    }
}